#!/usr/bin/env node

const argparse = require('argparse')
const create   = require('..')

meta = require('../package.json')
var cli = new argparse.ArgumentParser({
  prog:     meta.name,
  version:  meta.version,
  addHelp:  true
})

var subparsers = cli.addSubparsers({
  dest: 'command'
})

// Create command
var createParser = subparsers.addParser('create', {addHelp: true})
createParser.addArgument([ '--templatedir' ], {
  help:     'Template directory',
  defaultValue:  './templates'
})
createParser.addArgument([ '--localedir' ], {
  help:     'Locale directory',
  defaultValue:  './locales'
})
createParser.addArgument([ '-s', '--staticdir' ], {
  help:     'Static directory',
  defaultValue:  './static'
})
createParser.addArgument([ '-o', '--outputdir' ], {
  help:     'Output directory',
  defaultValue:  './www'
})
createParser.addArgument([ '-l', '--langs' ], {
  help:     'Available languages',
  nargs:    '+',
})
createParser.addArgument([ '-d', '--defaultLang' ], {
  help:     'Default language',
  defaultValue:  'en'
})

// New command
var newParser = subparsers.addParser('new', {addHelp: true})
newParser.addArgument([ '--templatedir' ], {
  help:     'Template directory',
  defaultValue:  './templates'
})
newParser.addArgument([ '--localedir' ], {
  help:     'Locale directory',
  defaultValue:  './locales'
})
newParser.addArgument([ '-s', '--staticdir' ], {
  help:     'Static directory',
  defaultValue:  './static'
})
// Other commands
// ...


var args = cli.parseArgs()

switch( args.command ){
  case 'new':
    const fs = require('fs')

    var callback = (e) => {
      if(e && e.code == 'EEXIST'){
      }
    }
    fs.mkdir(args.templatedir, callback)
    fs.mkdir(args.localedir,   callback)
    fs.mkdir(args.staticdir,   callback)

    console.log('Creating folder structure for new site:')
    console.log('   Making templates folder:      '  + args.templatedir)
    console.log('   Making locales folder:        '  + args.localedir)
    console.log('   Making static files folder:   '  + args.staticdir)
    break

  case 'create':
    console.log('Creating site:')
    console.log('   Taking templates from:      '  + args.templatedir)
    console.log('   Taking locales from:        '  + args.localedir)
    console.log('   Taking static files from:   '  + args.staticdir)
    console.log('   Dumping ouput to:           '  + args.outputdir)
    create( args.templatedir,
            args.staticdir,
            args.outputdir,
            args.localedir,
            args.langs,
            args.defaultLang )
    break
  case 'other':
    console.log('Not implemented yet')
    break
}
