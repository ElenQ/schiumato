# Schiumato Translatable Static Site Generator

This is a Static Site Generator for automatically translated webpages.

This tool takes Nunjucks templates and locale files and renders them in files,
following this structure:

- OutputDirectory/
    - language-1/
        - HTML files
    - language-2/
        - HTML files
    - ...


The site generator is very flexible. The only preconditions are the following:

- Templates starting with `.` or `_` are not rendered. They are considered base
  templates for inheritance.

- It uses `i18n_yaml` for localization.

    - It saves all the catalogs in a locale directory (selected by user).

- If there are folders in the Template folder they are maintained in the final
  website directory tree.

- It maintains templates' extension, this means it can render **any** filetype.
  In `examples` folder you can see a RSS feed example.

## Instalation

This module can be installed via NPM, it prefers global:

```
npm install -g schiumato
```

It can be also run from source, but not recommended, download it and run `npm
install` to download all the dependencies. Schiumato script is located in
`bin/` directory.


## Usage

Call `schiumato` script with `new` command to create the default directory
structure in the current folder.

Use `create` command to create the final website from the templates.

```
schiumato create [ARGS]
```

Run `--help` to see the help message.

More TBD.

## Template information

Any `nunjucks` template is valid. With these points to consider:

### Translation support

All the templates are rendered with an extra filter, called `translate`. It is
used to translate multiline messages. Like this:

``` html
<p>
  {% filter translate %}
  This is a message I want to translate.
  It's multiline.
  It's cool because it can contain some <i>HTML</i> inside.
  {% endfilter %}
</p>

```

Templates also have `_` function to translate short strings. Like this:

``` html
<p>{{ _('Translate this short stuff') }}</p>

<!-- Both translations work on javascript also -->
<script>
  alert( {{ _('translated string') }} );
</script>

```

They also contain `pageName`, `lang`, `default_lang` and `all_langs` variables,
containing the current template name, current language name,

### Exported variables

In each template you can access this variables:

#### For links and menus

- `pageName`: url to the current template from the language folder. To make the
  full path add `lang` variable like this: `/{{lang}}/{{pageName}}`
- `lang`: current language.
- `default_lang`: the default language of the site. It doesn't change.
- `all_langs`: all the languages the site is going to be translated to. It
  doesn't change

They are useful on links and menus:

``` html
<!-- example for a language changing menu -->
{% for l in langs %
  <a href="/{{l}}/{{pageName}}"><img src="/static/flags_{{l}}.jpg" /></a>
{% endfor %}

<!-- link to other pages, always preceed the language to make it consistent -->
<a href="/{{lang}}/about.html">{{_("About")}}</a>
<!-- or use relative paths -->
<a href="about.html">{{_("About")}}</a>

```

#### For feeds

- `current_date`: A JavaScript Date object with the content of the current
  date. Useful for for RSS feeds when used like:  
  `<pubDate>{{current_date.toUTCString()}}</pubDate>`
- `date_parse` function: A function that helps user create Date objects from
  text. Useful to convert the post's date to a UTCString for RSS feeds.

### Markdown support

There's also a Markdown rendering filter provided by `markdown-it`.

It can be used in a filter or in a short function:

``` html
{% filter markdown %}
{% filter translate %}
Filters can be *chained* to make it translate after markdown is rendered.
Isn't it **crazy**?
{% endfilter %}
{% endfilter %}

{{ md("You can also make *short* filters with `md`") }}

```

Markdown also supports internal HTML tags.

### Under development

This tools are currently under development and are likely to change in the near
future, you can use it at the moment but be aware.

#### List dir filtered by extension

All the templates have the `dir` function which can be used to list a directory
and get all the files on it which don't start with `_` or `.`.

> NOTE: `dir` must use the relative path from the templatedir.


``` html
{% for i in dir('blog', '.html') %}
  {{ i }} -> prints "blog/FILENAME"
{% endfor %}
```

This can be used with nunjucks' `import` statement to retrieve global values of
the templates.

It's useful for summaries and blogs but it's not 100% useful because **each
file must export it's link if it's needed**.

Check the `examples` folder in order to see it working.

## TODO

It's still in a PoC status. It will be developed while it's used for a real
project so don't expect this to work right now.

- [X] Handle shared `static` folder also from the script. Now it have to be
  done by hand.
    - [ ] When static file is deleted delete it from the result.

Ideas and comments are always welcome.

## What's new?

- **Version 1.1.0**:
    - **Markdown support!**: Use `filter markdown` or `md()` function for
      that.

- **Version 1.1.12**:
    - **Markdown**: Support for HTML tags inside the MarkDown.

- **Version 1.1.14**:
    - **Date utilities!**: Simple date parsing utilities for templates.

## Note

If i18n is needed for JavaScript, the localized part *must* be put on the HTML
template to be translated. This may change in the future. But it's not a
priority now.
