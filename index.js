const Renderer = require('./lib/renderer')
const fs       = require('fs')
const path     = require('path')
const _        = require('underscore')
const ncp      = require('ncp').ncp
const mkdirp   = require('mkdirp')

function create(templatedir, staticdir, outdir, localedir, langs, defaultlang){
  // TODO use defaultlang in future options
  var self = this

  // Check if output directory exists and if it doesn't, create it
  if(! fs.existsSync(outdir))
    fs.mkdirSync(outdir)
  else
    if(! fs.statSync(outdir).isDirectory() )
      throw "Output directory error \'"+outdir+"\' : File exists and it's not a directory"

  var r = new Renderer( templatedir, localedir, langs, defaultlang )

  // Render all the templates in the output directory:

  /*
   * Structure:
   * lang:
   *    template:
   *      contents
   *    template:
   *      contents
   *    ...
   * lang:
   *    ...
   * ...
   */
  _.chain(r.langs)
    .map(  (l) => [l, r.renderAll(l)] )
    .object()
    .each( (temps, l) => {
      // Create directory if it doesn't exist
      var dir = path.join(outdir, l)
      if(! fs.existsSync(dir))
        fs.mkdirSync(dir)
      else
        if(! fs.statSync(dir).isDirectory())
          throw "Target directory error \'"+dir+"\' : File exists and it's not a directory"

      _.each( temps, (content, name) =>{
        mkdirp.sync( path.dirname( path.join(dir, name) ))
        fs.writeFile(
          path.join(dir, name),
          content,
          (e) => {
            if(e)
              throw e;
          })
      })
    })
  r.end()

  // Create static directory in www with the same name:
  var outstatic = path.join(outdir, path.basename(staticdir))
  //mkdirSync( outStatic )
  // Dump the content
  ncp(staticdir, outstatic,  function (err) {
    if (err)
      throw "Error while copying Static folder: " + err
  })
}

module.exports = create
