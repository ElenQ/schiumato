const nunjucks = require('nunjucks')
const I18n     = require('i18n_yaml')
const fs       = require('fs')
const path     = require('path')
const findall  = require('recursive-readdir-sync')
const _        = require('underscore')
const md_it    = require('markdown-it')({ html: true })

function removeExt(inpath){
  return inpath.substring(0, path.lastIndexOf('.'))
}
function url(inpath){
  return encodeURI(inpath.split(path.sep).join('/'))
}

Renderer = function(templatedir, localedir, langs, defaultLang){
  if(!localedir)
    throw 'Locale directory is mandatory'
  if(!templatedir)
    throw 'Template directory is mandatory'
  this.localedir    = localedir
  this.templatedir  = templatedir

  // Create the translator
  this.translator = new I18n({
    locales:        langs,
    localedir:      this.localedir,
    clean:          true
  })
  this.langs        = langs         || this.translator.getAvailableLocales()
  this.defaultLang  = defaultLang   || _.first(this.langs)


  // Configure template engine
  this.env = new nunjucks.configure(
    this.templatedir,
    {
      noCache: true,
      autoescape: false,
    })

  // Translation filter
  var trans = this.translator.prepare()
  var translate = function(message){
    if(message instanceof nunjucks.runtime.SafeString)
      message = message.toString()
    return new nunjucks.runtime.SafeString(trans(message))
  }
  this.env.addFilter('translate', translate)

  // Markdown filter
  var md = function(message){
    if(message instanceof nunjucks.runtime.SafeString)
      message = message.toString()
    return new nunjucks.runtime.SafeString(md_it.render(message))
  }
  this.env.addFilter('markdown', md)

  // List all files in directory which match the extension
  // NOTE: Needs the path relative to the templatedir directory
  var dir = function(absdir, ext){
    var files = _.chain(findall(path.join(templatedir, absdir)))
                .filter( (t) => !path.basename(t).startsWith('.') )
                .filter( (t) => !path.basename(t).startsWith('_') )
                .map(    (t) => path.relative(templatedir, t))
                .value()
    if( ext )
      return _.filter(files, (f) => path.extname(f) == ext )
    return files
  }

  // Create default context for the templates
  this.env.addGlobal('default_lang', this.defaultLang)
  this.env.addGlobal('all_langs', this.langs)
  // Functions
  this.env.addGlobal('md', md)                          // Markdown Rendering
  this.env.addGlobal('_', translate)                    // Translation
  this.env.addGlobal('dir', dir)                        // List directory
  this.env.addGlobal('current_date', new Date())        // Current date
  this.env.addGlobal('date_parse', (d)=>(new Date(d)))  // Date parsing helper
}

Renderer.prototype.renderTemplate = function (name, lang){
  if(! lang in this.langs)
    throw "Trying to render an unconfigured language"
  this.translator.setCurrentLocale(lang)
  var template = this.env.getTemplate(name)

  // insert individual context
  var context = {
                  lang: lang,
                  pageName: url(name)
                }
  return template.render( context )
}

Renderer.prototype.renderAll = function(lang){
  /* Renders all the templates in the templates directory recursively.
   *
   * Returns:
   * { template: contents ... }
   */
  var self = this
  return _.chain( findall(self.templatedir) )
    .filter( (t) => !fs.lstatSync(t).isDirectory())
    .map(    (t) => path.relative(self.templatedir, t))
    .filter( (t) => !path.basename(t).startsWith('.') )
    .filter( (t) => !path.basename(t).startsWith('_') )
    .map(    (t) => [ t, self.renderTemplate(t, lang)] )
    .object()
    .value()
}

Renderer.prototype.end = function (){
  console.log('Dumping catalogs...')
  this.translator.updateCatalogs()
  console.log('Ending...')
}

module.exports = Renderer
